require "spec_helper"
describe "View list of companies" do
	it "shows a header" do
		visit companies_path
		expect(page).to have_text("Companies")
	end

	it "shows a company" do
		company1 = Company.create(
			name: "testName",
			ceo: "testCEO",
			address: "testAddress",
			state: "testState",
			zip: "testZip")

		visit companies_path
		expect(page).to have_text("testName")
		expect(page).to have_text("CEO: " + company1.ceo)
		expect(page).to have_text("Address: " + company1.address)
		expect(page).to have_text("State: " + company1.state)
		expect(page).to have_text("Zip: " + company1.zip)

	end

end