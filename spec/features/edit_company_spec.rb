require "spec_helper"
describe "Edit a company" do
  it "displays change in index" do
    company1 = Company.create(company_attribute)
    visit companies_path
    expect(page).to have_text(company1.name)
    first(:link, "Edit").click
    fill_in('company_name', :with => "testName" )
    click_on "Update Company"
    expect(current_path).to eq(companies_path)
    expect(page).to have_text("testName")
  end
  it "shows an error on bad edit" do
    company1 = Company.create(company_attribute)
    visit companies_path
    expect(page).to have_text(company1.name)
    first(:link, "Edit").click
    fill_in('company_name', :with => "" )
    click_on "Update Company"
    expect(page).to have_text("Name can't be blank")
  end
  
end