require "spec_helper"
describe "Adding a company" do 
	it "shows in the index" do
		visit companies_path
		click_link "Add New Company"
		expect(current_path).to eq(new_company_path)

		company1 = Company.create(company_attribute)
		fill_in('company_name', :with => company1.name)
		fill_in('company_ceo', :with => company1.ceo)
		fill_in('company_address', :with => company1.address)
		fill_in('company_state', :with => company1.state)
		fill_in('company_zip', :with => company1.zip)
		click_on "Create Company"
		expect(current_path).to eq(companies_path)
		expect(page).to have_text(company1.name)
		expect(page).to have_text(company1.ceo)
		expect(page).to have_text(company1.address)
		expect(page).to have_text(company1.state)
		expect(page).to have_text(company1.zip)
	end

	it "missing field shows an error" do
		visit companies_path
		click_link "Add New Company"

		company1 = Company.create(company_attribute)
		fill_in('company_name', :with => company1.name)
		click_on "Create Company"

		expect(page).to have_text("Ceo can't be blank")
		expect(page).to have_text("Address can't be blank")
		expect(page).to have_text("State can't be blank")
		expect(page).to have_text("Zip can't be blank")
		
	end
end