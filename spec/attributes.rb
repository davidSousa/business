def company_attribute(overrides = {})
	{
		name: "testName",
		ceo: "testCEO",
		address: "testAddress",
		state: "testState",
		zip: "testZip"
	}.merge(overrides)
end