class Company < ActiveRecord::Base
	validates :name, :ceo, :address, :state, :zip, presence: true	
end
